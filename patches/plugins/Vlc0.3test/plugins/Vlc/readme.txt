

=================================================================================================

THIS PLUGIN WON'T WORK UNTIL YOU COPY SOME FILES !!! See below in the section surrounded by +++++

=================================================================================================


The Vlc vvvv-plugin is based on 2 other projects, namely nVlc and LibVLC.

This might mean that this code itself falls under the GPL too, I don't really know. Anyway the GPL can be found here: http://www.gnu.org/copyleft/gpl.html.

The reason I release this is just to check if it works for other people, 
and I intend to add the source code under SVN on sourceforge to be added 
in the plugin package, if it seems that this plugin is becoming stable 
and useful for other people than myself.

If you can't wait to see the messy source-code, and you think you have the 
right to see it, contact me about it...
If it's not that urgent, have some patience, give me some more time to get 
everything organised, and let me know if it works for you.


ABOUT THE Vlc PLUGIN:

* Written by Frederik Tilkin in 2010-2011
* Please check out VLC's wiki pages to see how to use filters, and directshowsources etc. 
  You can open any source in VLC player, and use the same MRL + options (tick 'show more options' in the 'Open Media' dialog), 
  except you have to separate every argument by a | (replace the : by |)
* A useful starting point might be http://wiki.videolan.org/Documentation:Play_HowTo/Advanced_Use_of_VLC


ABOUT THE LIBRARIES USED:

nVlc is a .net wrapper library for the libvlc video decoder library:

The following dll's belong to nVlc (http://www.codeproject.com/KB/audio-video/nVLC.aspx):
- Declarations.dll
- Implementation.dll
- LibVlcWrapper.dll
- NLog.dll

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
In order for it to work, you need to copy some files 
from your "VLC player" installation folder (http://www.videolan.org/) 
to the same directory as the nVlc dll's:
- libvlc.dll
- libvlccore.dll
- the full plugin directory

Any version 1.1.X of VLC player should work. As soon as VLC 1.2 is out, I will try to update...
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



REMARK: I made 1 small change to the nVlc version of september 2010:

	* in LibVlcMethods.cs I changed

	[DllImport("libvlc")]
	public static extern int libvlc_media_get_tracks_info(IntPtr media, ref libvlc_media_track_info_t tracks);

	to

	[DllImport("libvlc")]
	public static extern int libvlc_media_get_tracks_info(IntPtr media, out IntPtr tracks);

	since the original function is libvlc_media_get_tracks_info(IntPtr media, libvlc_media_track_info_t** tracks);


