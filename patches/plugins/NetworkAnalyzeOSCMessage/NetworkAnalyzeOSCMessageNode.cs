#region usings
using System;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Collections.Generic;
using System.Collections;
using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VMath;
using VVVV.Utils.OSC;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{



	#region PluginInfo
	[PluginInfo(Name = "AnalyzeOSCMessage", Category = "Network", Help = "Basic Node to manage the Vertigo Interface along with the OSC-Messages", Tags = "Iris, v4Bundle, Parameter")]
	#endregion PluginInfo

	public class NetworkAnalyzeOSCMessageNode : IPluginEvaluate
	{

		#region fields & pins
		string dstringORdXYZ = "";
		bool defaultOrOsc = true;
		bool IsString = false;
		bool hold = false;
		bool FListening = false;
		List<string> FDecodedOSCMessages = new List<string>();


		[Input("OSCMessages", DefaultString = "MyOSCMessage")]
		IDiffSpread<string> FOSCMessages;


//-------------------------------------



		[Output("Found Adresses")]
		ISpread<string> FFoundAddresses;

		[Output("Is Bundle")]
		ISpread<bool> FIsBundle;
		
		[Output("TypeTag")]
		ISpread<string> FTypeTag;

		[Output("Arguments")]
		ISpread<string> FArguments;

		[Output("Arguments Bin Size")]
		ISpread<int> FArgumentsBinSizes;

		[Output("Debug")]
		ISpread<string> FDebug;


		[Import()]
		IPluginHost FHost;
		[Import()]
		ILogger Flogger;

		#endregion fields & pins


		List<string> MyAdresses = new List<string>();
		List<bool> IsBundle = new List<bool>();
		List<string> Arguments = new List<string>();
		List<int> ArgumentsBinSizes = new List<int>();

		private OSCReceiver FOSCReceiver;


		private byte[] StringToByteArray(string str)
		{
			return System.Text.Encoding.GetEncoding(1252).GetBytes(str);
		}

		private void ListenToOSC()
		{

			for (int i = 0; i < FOSCMessages.SliceCount; i++) {

				OSCPacket packet = OSCPacket.Unpack(StringToByteArray(FOSCMessages[i]));
				{
					if (packet != null) {
						
						if (packet.IsBundle()) {
							IsBundle.Add(true);
							ArrayList messages = packet.Values;
							for (int j = 0; j < messages.Count; j++) {
								ProcessOSCMessage((OSCMessage)messages[j]);
								//Flogger.Log(LogType.Debug, "packet is bundle");
							}
						} else {
							IsBundle.Add(false);
							ProcessOSCMessage((OSCMessage)packet);
							//Flogger.Log(LogType.Debug, "packet isnt bundle");
						}

					} else
						Flogger.Log(LogType.Debug, "packet is not okay. either address mismatch or message is fucked up");

				}
			}

		}
		private void ProcessOSCMessage(OSCMessage message)
		{
			string address = message.Address;
			ArrayList args = message.Values;
//			OSCMessage MyMessage;
//			string MyTypeTag = MyMessage.typeTag();
				for (int i = 0; i < args.Count; i++) {
					Arguments.Add(Convert.ToString(args[i]));
				}

			MyAdresses.Add(address);
			ArgumentsBinSizes.Add(args.Count);

		}



		public void Evaluate(int SpreadMax)
		{
			FDebug.SliceCount = 1;
			MyAdresses.Clear();
			IsBundle.Clear();
			Arguments.Clear();
			ArgumentsBinSizes.Clear();
			if (FOSCMessages.SliceCount > 0) {
				ListenToOSC();

			}
			FFoundAddresses.AssignFrom(MyAdresses);
			FIsBundle.AssignFrom(IsBundle);
			FTypeTag.SliceCount=1;
			FTypeTag[0]="Not ready yet";
			FArguments.SliceCount = Arguments.Count;
			FArguments.AssignFrom(Arguments);
			FArgumentsBinSizes.AssignFrom(ArgumentsBinSizes);
		}
	}
}
