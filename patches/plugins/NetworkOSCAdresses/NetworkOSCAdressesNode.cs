#region usings
using System;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Collections.Generic;
using System.Collections;
using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VMath;
using VVVV.Utils.OSC;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{



	#region PluginInfo
	[PluginInfo(Name = "OSCAdresses", Category = "Network", Help = "Returns OSC Address in a OSC Message", Tags = "OSC, OSC Address, Network, Analyze")]
	#endregion PluginInfo

	public class NetworkOSCAdressesNode : IPluginEvaluate
	{

		#region fields & pins
	
		[Input("OSCMessages", DefaultString = "MyOSCMessage")]
		ISpread<string> FOSCMessages;


//-------------------------------------



		[Output("Found Adresses")]
		ISpread<string> FFoundAddresses;


		[Import()]
		IPluginHost FHost;


		#endregion fields & pins

		bool ignore;
		List<string> MyAdresses = new List<string>();
		private OSCReceiver FOSCReceiver;


		private byte[] StringToByteArray(string str)
		{
			return System.Text.Encoding.GetEncoding(1252).GetBytes(str);
		}

		private void ListenToOSC()
		{
		
			for (int i = 0; i < FOSCMessages.SliceCount; i++)
			{
					OSCPacket packet = OSCPacket.Unpack(StringToByteArray(FOSCMessages[i]));
					if (packet != null) 
					{
						if (packet.IsBundle()) 
						{
							ArrayList messages = packet.Values;

							for (int j = 0; j < messages.Count; j++) 
								ProcessOSCMessage((OSCMessage)messages[j]);							
						} else 
							ProcessOSCMessage((OSCMessage)packet);
					}
			
			}

		}
		private void ProcessOSCMessage(OSCMessage message)
		{
			string address = message.Address;
			MyAdresses.Add(address);

		}



		public void Evaluate(int SpreadMax)
		{
			MyAdresses.Clear();
		
			if (FOSCMessages.SliceCount > 0) {
				ListenToOSC();

			}
			FFoundAddresses.AssignFrom(MyAdresses);
		}
	}
}
